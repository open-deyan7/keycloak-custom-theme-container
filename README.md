# keycloak-custom-theme-container

## Custom theme development
### Prerequesites and start Keycloak
- Download the suiting Keycloak version as zip file from https://github.com/keycloak/keycloak/releases
- Unpack it, navigate it via Terminal into the folder
- Run ` bin/kc.sh start-dev`
`start-dev` will disable caching and other logic, so you will instantly be able to see your changes.

You now have a local Keycloak installation running that uses a in-memory database. All configured data is lost after stop or restart. Thus we recommend to leave the installation running for the whole development process (or to configure Keycloak to use a PostgreSQL database, e.g.).  

If you plan on developing an email template, you should insert some valid email sender credentials to your Realm configuration.

### Create custom theme
If you navigate to `lib/lib/main/org.keycloak.keycloak-themes-x.y.z.jar` and unzip this jar, you can find the predelivered themes in there. This will help as an inspiration or to know how it´s working.  

Generally it is recommended to extend from a theme and only change what is needed for your case.

For further documentation about how to create a custom theme and extending an existing theme, look here: https://www.keycloak.org/docs/latest/server_development/#creating-a-theme.  

__Please be aware that most of the documentation does not apply to email templates!__  

E.g. you cannot use external CSS for email templates.

### Development process
After placing your theme in the correct folder, make changes to the theme and trigger the email sending.  
It will speed up development turnarounds if you first implement the HTML via Editor+Browser, then minify the HTML (e.g. via https://minify-html.com) and place it into the .ftl file afterwards.

### Publishing
After the theme is implemented, build a Docker image via the supplied `Dockerfile` and publish the image to a Container registry of your choice. It can then be injected into the Keycloak deployment.